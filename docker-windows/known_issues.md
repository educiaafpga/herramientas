# Problemas conocidos en windows hasta la fecha. 


## Problemas con la instalacion del WSL.
Al momento de instalar el WSL este falla con el siguiente error: 

```powershell
wslregisterdistribution failed with error: 0x80370114
```

Esto se debe a que no se activo hyper-v, es posible hacerlo desde la linea de comandos siguiendo la guia o desde el menu de activar o desactivar caracteristicas, si el sistema operativo esta en español, el hyper-v se llama hipervisor.

## Problemas con docker.

Si al momento de utilizar el docker tenemos un error de este tipo (Y sabemos que el docker esta instalado):

```powershell
docker not recognized as a command windows
```
Es posible que nuestro usuario no tenga acceso a docker, esto es posible de solucionar de dos formas. 

- Si se esta utilizando desde la terminal, ejecutar la terminal como administrador 
- Si se esta ejecutando como usuario, agregar el path de instalacion de docker (la carpeta bin dentro del directorio de docker) a PATH de esta forma el sistema sabe donde encontrar ese programa. 

