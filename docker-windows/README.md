# Instalación Herramientas en Windows

Probado en :

- Windows 10 Pro 64 bits Build 19041 y 18363
- Windows 10 Home 64 bits Build 1903

## Docker Desktop for Windows

Docker utiliza virtualización en el Sistema Operativo Anfitrión o _Host_ para correr software en contenedores. En Windows esta virtualización puede ser realizada con distintos _backends_:

- WSL 2
- Hyper-V
- Virtual Machine (legacy)

Para esta guía nos vamos a focalizar en la utilización de WSL 2 por su simpleza y el hecho que es el método más optimizado y reciente. Las otras alternativas también son teoricamente posibles, pero queda a cargo del lector investigar un poco más sobre ellas.

### WSL 2

[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about) es una capa de compatibilidad que permite correr archivos binarios ejecutables de Linux nativamente en Windows 10. WSL 2 fue lanzado en Junio 2019 e introdujo un kernel de Linux real a traves de la utilización de un subset de funcionalidades optimizadas de Hyper-V. Docker funciona bastante bien y de manera muy simple cuando utilizamos este backend.

Los requerimientos del sistema son los siguientes:

- For x64 systems: Version 1903 or higher, with Build 18362 or higher.
- For ARM64 systems: Version 2004 or higher, with Build 19041 or higher.
- Builds lower than 18362 do not support WSL 2. Use the Windows Update Assistant to update your version of Windows.

Cabe destacar que este backend ***solo es compatible con computadoras con versiones de Windows de 64bits***. ARM64 también está soportado, aunque no se ha probado la compatibilidad del resto de las herramientas en dicha plataforma.

De cumplir con los mismos proceder con la instalación. Para instrucciones detalladas referirse a [la guía oficial en la documentación de Microsoft](https://docs.microsoft.com/en-us/windows/wsl/install-win10):

- Abrir una instancia de Power Shell como Administrador

- Activar el Hyper-V:
 ```bash
DISM /Online /Enable-Feature /All /FeatureName:Microsoft-Hyper-V
 ```
 - Instalar WSL:
 ```bash
wsl --install
 ```
- No reiniciar la computadora todavia 

### USBPID

La herramienta USBPID nos va a permitir compartir con el WSL los puertos fisicos de nuestra PC siguiendo los pasos que estan **[aca](https://learn.microsoft.com/en-us/windows/wsl/connect-usb)**

  - Descargar el archivo usbpid-win_3.1.0.msi [desde este enlace](https://github.com/dorssel/usbipd-win/releases/download/v3.1.0/usbipd-win_3.1.0.msi)
  - Ejecutarlo
  - **Reiniciar la computadora** (Ahora si)
  - Al iniciar deberia aparecer una terminal en la que se ejecuta el WSL, ahi se le pedira al usuario que se registre con un nombre de usuario y una contraseña para el Linux. 

  - De no aparecer solo, entrar al entorno del WSL2 corriendo:

```bash
    wsl
```

  - Ahora debemos actualizar el sistema.

  - Ejecutar las siguientes lineas:
  
```bash
    sudo apt-get update --fix-missing
    sudo apt install linux-tools-generic hwdata
    sudo update-alternatives --install /usr/local/bin/usbip usbip /usr/lib/linux-tools/*-generic/usbip 20
```

  - Salir del WSL.

- Listo, ahora ya esta listo nuestro WSL. 


### Instalación Docker Desktop for windows

Hay una [guía en la página de Docker](https://docs.docker.com/docker-for-windows/install/) en donde esta detallado el proceso, pero a continuación les voy a describir los pasos para su conveniencia. De encontrarse con algún problema acudir a la documentación oficial puede ser un buen comienzo para solucionarlo.

Los siguientes pasos son para PCs que tengan Windows 10 profesional (Build 16299 y posteriores). De tener Windows 10 Home referirse al siguiente [link](https://docs.docker.com/docker-for-windows/install-windows-home/) y seguir las instrucciones.

- Descargar la versión estable de Docker Desktop de la página oficial -> [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/)
- Proceder con la instalación
  - Dejar tildadas las casillas de verificación. Hyper-V es opcional si se está utilizando WSL 2 como backend.
- Si no habiamos instalado WSL 2 antes de llegar hasta este paso nos preguntará si queremos hacerlo.
- Verificar si se agrego docker a path, tanto en la variables de entorno del usuario como en las del sistema deberia aparecer (y si no es asi agregarlo) el path a la carpeta de instalacion de docker, por ejemplo "C:\Program Files\Docker\Docker\resources\bin"

### Verificación de la instalación

Para corroborar que todo este funcionando correctamente abriremos una ventana de línea de comandos en Windows (cmd) y corremos el siguiente comando:
```bash
docker run hello-world
```
Si todo funciona ok veremos el siguiente mensaje:

> Hello from Docker!
This message shows that your installation appears to be working correctly.
>
>To generate this message, Docker took the following steps:
>
> 1. The Docker client contacted the Docker daemon.
> 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
>    (amd64)
> 3. The Docker daemon created a new container from that image which runs the
>    executable that produces the output you are currently reading.
> 4. The Docker daemon streamed that output to the Docker client, which sent it
>    to your terminal.

## Descargar imagen oficial para el entorno : [educiaafpga/x64](https://hub.docker.com/r/educiaafpga/x64)

Esta imagen contiene todas las herramientas necesarias para usar el entorno:

- [GHDL](http://ghdl.free.fr/) para compilacion y simulacion de VHDL
- [Icarus Verilog](http://iverilog.icarus.com/) para compilacion y simulacion de Verilog
- [VUnit](https://vunit.github.io/) para Unit Testing con VHDL
- [YOSYS](http://www.clifford.at/yosys/) para sintesis
- [NextPNR](https://github.com/YosysHQ/nextpnr) para Place and Route
- [iCEstorm](http://www.clifford.at/icestorm/) para configurar la EDU-CIAA-FPGA
- [Python 3.7](https://www.python.org/) para ejecutar herramientas auxiliares

Descargarla es tan sencillo como:
```bash
docker pull educiaafpga/x64
```

## Instalación GtkWave en Windows

- [Descargar GtkWave desde SourceForge](https://sourceforge.net/projects/gtkwave/files/gtkwave-3.3.100-bin-win64/gtkwave-3.3.100-bin-win64.zip/download)
- Descomprimir el archivo "gtkwave-x.x.xxx-bin-win64.zip" en un lugar seguro que no vayamos a ir cambiando de sitio frecuentemente
  - Para este ejemplo elegi guardarlo en el root del disco C:, lugar donde tengo instalado mi Windows.
- Para poder ejecutar gtkwave desde la consola de Windows sin tener que andar especificando el directorio donde lo tenemos instalado necesitaremos modificar la variable de entorno PATH de nuestro Windows.
  - Tipear en la barra de búsqueda "Configuración de Sistema Avanzada" ó "Advanced System Settings", dependiendo del idioma de nuestro Windows.
    - Alternativamente acceder desde el panel de control. Windows esta desfazando el panel de control así que no podemos asegurar por cuanto tiempo hacer esto seguirá siendo una opción válida.
    - En algunas versiones en cambio el menu se llama "Editar las variables de entorno del sistema"
  - Hacer click  en "Variables de Entorno" o "Environment Variables"
  - Click en "PATH" y luego en "Editar"
    - En esta nueva ventana podremos agregar el directorio donde se encuentra nuestro ejecutable de gtkwave. Para este ejemplo, como copiamos nuestra carpeta de GtkWave al root del disco C:, el directorio será:
  ```bash
  C:\gtkwave\bin
  ```
  - Click en "Nuevo"
    - Completar el campo con el directorio donde se encuentra gtkwave.exe
  - Abrir la consola (cmd.exe) y tipear "gtkwave" y apretar enter para comprobar el funcionamiento de la herramienta. Si todo está bien, el programa debería abrirse.

