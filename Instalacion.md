# Prologo
Esta pagina esta disponible en [la Wiki del proyecto](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Herramientas-de-Desarrollo)

---

El siguiente instructivo explica como instalar y utilizar las herramientas recomendadas para:
 - Compilar y simular código VHDL o Verilog
 - Sintetizar y configurar la placa EDU-FPGA

[[_TOC_]]

# Introducción

Actualmente existen infinidad de herramientas _open source_ para diseño, verificación e implementación de circuitos digitales en tecnología FPGA, tanto en VHDL como Verilog. Por nombrar algunas herramientas que se utilizan en esta Wiki : GHDL, icarusVerilog, YOSYS, OSVVM, iceStorm, gtkWave, entre otras.

Sin embargo, esto también supone un gran esfuerzo por parte del interesado en introducirse a la temática, que debe descargar e instalar todas estas herramientas por separado, lo cual acarrea los clásicos problemas de librerías y dependencias típicos de cualquier proceso de instalación en Linux. Además, todas estas herramientas carecen de una interfaz gráfica que facilite su uso para usuarios recién llegados.

Por estas razones se creó un entorno de desarrollo propio que facilite la instalación y utilización de estas herramientas, mediante la integración de dos aplicaciones con bastante madurez:

 - Por un lado, la plataforma **Docker** compone el _back-end_ del entorno. Como veremos más adelante, Docker es un gestor de aplicaciones autocontenidas o "contenedores". Se trata de pequeñas máquinas virtuales pero que solo contienen lo básico e indispensable para ejecutar determinadas herramientas en un entorno Linux, por lo cual son mucho más livianas que una imagen virtual tradicional.
 - Por el otro, el editor de codigo **VSCode** forma el _front-end_ o interfaz gráfica. Se trata de un editor personalizable con una amplia disponibilidad de extensiones mantenidas por la comunidad. Esto nos permitió agregar nuevas opciones para emplear las herramientas disponibles en los contenedores Docker directamente desde el entorno gráfico, sin necesidad de recurrir a una terminal.

![Blocks](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/a4958650342539bcdacf17371d05388b/Blocks.png)

A través de este tutorial se detalla como instalar y usar el entorno VSCode + Docker para tener un sistema de herramientas funcional. Este proceso es el recomendado ya que resulta mucho más rápido que instalar las herramientas por separado.

# Requerimientos

**Recomendamos utilizar un sistema operativo Linux**, ya sea [Ubuntu](https://ubuntu.com/) o [Linux Mint](https://www.linuxmint.com/). Pueden estar instalados sobre una partición en el disco duro o una máquina virtual. Por supuesto, varias de estas herramientas se instalan empleando la **Terminal** de Linux. Sin embargo, los comandos a utilizar se detallarán paso a paso, para aquellos usuarios con poca experiencia en este sistema. No obstante, **se puede usar también Windows 10** para estas herramientas. La única diferencia radica en las extensiones a utilizar y el proceso de instalación de Docker, como se menciona más adelante.

Para emplear el entorno de desarrollo propuesto necesitamos tener instalado:
- **VSCode** como editor de texto.
- **Docker** para ejecutar las herramientas de simulación y síntesis.
- **GTKWave** para visualizar las formas de onda resultantes.

## VSCode

[VSCode](https://code.visualstudio.com/) es un editor de texto desarrollado por Microsoft y mantenido tanto por la empresa como por una comunidad bastante amplia de desarrolladores. Tiene un aspecto muy similar a Atom, y esto se debe a que ambos emplean el mismo framework base, llamado [Electron](https://www.electronjs.org/), que permite desarrollar aplicaciones multiplataforma usando JavaScript, HTML y CSS (si bien VSCode emplea [TypeScript](https://www.typescriptlang.org/) para la creación de extensiones).

![VSCode](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/521dc24590b795eb970f4b15c31f3d5d/VSCode.png)

> IMPORTANTE : actualmente la integración con Docker solo funciona en Linux. Estamos desarrollando una adaptación para poder emplear la imagen Docker en Windows con VSCode.

### Instalación

Entrar a la página de [descargas](https://code.visualstudio.com/Download) y seleccionar el ejecutable que corresponda al sistema operativo que se posee.

- En Windows, hacer doble click en el ejecutable descargado y seguir los pasos de instalación
- En Linux, abrir un terminal en el directorio donde se descargó el ejecutable y hacer:

```bash
dpkg -i NOMBRE_DEL_INSTALADOR.deb
```

> Se necesita tener el paquete **dpkg** instalado. Puede ser necesario anteponer **sudo**.

### Como abrir

1. Ir al directorio que contiene el o los archivos que queremos editar
2. En Windows, hacer **click derecho > abrir en VSCode**
3. En Linux, abrir un terminal y hacer `code .`

### Extensiones recomendadas

En el [Marketplace](https://marketplace.visualstudio.com/VSCode) de VSCode podemos encontrar todo tipo de extensiones para agregar funcionalidades a nuestro editor. También podemos acceder al Marketplace desde VSCode yendo al menú **View/Extensions**:

![Marketplace](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/49e9934ed7cb0de104e8fd147841e696/MArketplace.png) 

Las que recomendamos para trabajar con VHDL y Verilog son:
- [VHDL](https://marketplace.visualstudio.com/items?itemName=puorc.awesome-vhdl) : syntax highlight para VHDL
- [Verilog-HDL](https://marketplace.visualstudio.com/items?itemName=mshr-h.VerilogHDL) : syntax highlight para Verilog y SystemVerilog.

Otras  extensiones recomendadas son:
 - [Markdown All-In-One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one): Previsualizar archivos Markdown desde VSCode
 - [WaveTrace](https://marketplace.visualstudio.com/items?itemName=wavetrace.wavetrace): Ver archivos de forma de onda (.vcd) en VSCode (puede funcionar como reemplazo de GtkWave)
 - [Serial Terminal](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-serial-monitor): Abrir un terminal serie y ver las tramas recibicas (util si se trabaja con UART en la FPGA)



### Agregando la extensión EDU-CIAA-FPGA

1. Descargar la extension en formato VSIX desde [este link](https://gitlab.com/educiaafpga/edu-ciaa-fpga-vscode-extension/-/jobs/artifacts/main/raw/educiaafpga.vsix?job=build-extension)
2. Abrir VSCode y presionar **Ctrl+Shift+P**. Esto abre el **Command Palette** de la aplicación.
3. Buscar el comando **Extensiones: Instalar desde VSIX**
4. Buscar el archivo VSIX descargado en el paso 1
5. Reiniciar o abrir VSCode y presionar **Ctrl+Shift+P**.
6. Ingresar **EDU-CIAA-FPGA**. Deberían aparecer las utilidades provistas por la extensión:

![VSCodePalette](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/ef71d0580099efa7915277c4f06fc0c2/VSCodePalette.png)

## Docker 

[Docker](https://www.docker.com/) es una herramienta diseñada para crear, desplegar y correr aplicaciones usando **contenedores**. Estos contenedores permiten a un desarrollador empaquetar una aplicación con todas las partes que necesite, tales como librerías, paquetes y otras dependencias. De esta manera, el desarrollador puede confiar que la aplicación correrá en cualquier otra máquina independientemente de la configuración local del sistema. Por su parte, esto libera al usuario final de instalar infinidad de dependencias y librerías requeridas.

Cada contenedor de Docker está basado en una **imagen** o plantilla que contiene previamente instaladas todas las herramientas y librerías necesarias para la aplicación. En nuestro caso, usaremos una serie de imágenes de libre acceso que ya contienen todas las herramientas necesarias para compilar, simular e implementar nuestros diseños digitales.

### Instalacion en Linux

Abrir una terminal y ejecutar lo siguiente:

#### Alternativa 1 (64 bits)
```bash
sudo apt-get update
sudo wget -qO- https://get.docker.com/ | sh

sudo usermod -aG docker $USER
docker --version
```

#### Alternativa 2 (64 bits)
```bash
sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release echo "$UBUNTU_CODENAME") stable"
sudo apt-get update
sudo apt-get -y  install docker-ce docker-compose

sudo usermod -aG docker $USER
docker --version
```

#### Alternativa 3 (32 bits)
```bash
sudo wget https://gitlab.com/docker-32bit/ubuntu/raw/master/build-image.sh
sudo bash build-image.sh

sudo usermod -aG docker $USER
docker --version
```
### Instalación en Windows

Para instalar Docker en Windows 10 se necesita seguir un procedimiento bastante mas extenso, el cual se encuentra detallado en [esta pagina](https://gitlab.com/educiaafpga/herramientas/-/tree/master/docker-windows).

### Descargando imágenes desde Docker Hub

Docker Hub es una plataforma online libre desde la cual se pueden descargar una gran cantidad de imágenes, cada una de las cuales es mantenida de forma activa por sus desarrolladores y contiene las librerías y herramientas específicas para una aplicación. 

![DockerHub_Explore](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/9a0ad25f9291584cb87d7a5082a1ad72/DockerHub_Explore.png)

### Descargar la imagen necesaria para el entorno

Como primer paso, vamos a agregar nuestro usuario al grupo de usuarios de Docker:
```bash
sudo usermod -aG docker $USER
```

Como parte del proyecto EDU-CIAA-FPGA se ha desarrollado una **Imagen Docker Oficial** que contiene todas las herramientas necesarias para compilar, simular y sintetizar código en VHDL o Verilog. Dicha imagen puede encontrarse [aquí](https://hub.docker.com/r/educiaafpga/x64) y descargarse abriendo un terminal y haciendo:
```bash
docker pull educiaafpga/x64
```

> Actualmente esta imagen es solo para sistemas de 64 bits. De ser necesario agregaremos una alternativa para 32 bits.

## GtkWave

[GtkWave](http://gtkwave.sourceforge.net/) es un programa de visualización de formas de onda. Lo utilizaremos para ver los resultados de simulaciones de diseños digitales realizados en VHDL o Verilog. 

![Gtk-Wave](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/91c9a3946743412e84d8ab86ad367232/gtkWave.png)

Al tratarse de una herramienta gráfica, no podemos ejecutarla desde un Docker, así que debemos instalarla en nuestro sistema operativo. Sin embargo, es tan fácil como abrir un Terminal y ejecutar:

```bash
sudo apt-get install -y gtkwave
```

Para Windows, pueden encontrarse instaladores en la página oficial del software.

# Lecturas recomendadas
El repositorio [FOSS for FPGA](https://github.com/rodrigomelo9/FOSS-for-FPGAs) de Rodrigo Melo contiene excelente material sobre como usar Docker y herramientas libres para el desarrollo en FPGA. Te invitamos a visitarlo !

[^1]: Esta es la herramienta utilizada para mostrar el flujo de diseño digital para la FPGA Lattice iCE40 en el resto de la Wiki.
