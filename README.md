[[_TOC_]]

# Repositorio de herramientas para el proyecto EDU-CIAA-FPGA
Este repositorio contiene herramientas desarrolladas por el equipo de trabajo o aportadas por la comunidad que ofrecen distintas utilidades para el diseño digital, simulación, síntesis y configuración de la placa EDU-CIAA-FPGA.

# Lo que hay hasta ahora
 - **edu-ciaa-fpga-vscode-extension** : Extensión oficial EDU-CIAA-FPGA embebida como un submódulo externo.
 - **docker-windows** : Instrucciones y recursos para usar Docker en Windows 10
 - **dockerfiles** : Instrucciones y archivos fuente para compilar las imagenes Docker del proyecto.

# Sobre la instalacion de herramientas
Leer [este documento](Instalacion.md)

# Cómo clonar este repositorio

Ademas de `git clone`, es necesario usar `git submodule` para descargar recursivamente los submodulos:

```
git clone https://gitlab.com/educiaafpga/herramientas.git
cd herramientas
git submodule update --init --recursive
```

# Descargar la ultima version de la extension EDU-CIAA-FPGA

[LINK](https://gitlab.com/educiaafpga/edu-ciaa-fpga-vscode-extension/-/jobs/artifacts/main/raw/educiaafpga.vsix?job=build-extension)