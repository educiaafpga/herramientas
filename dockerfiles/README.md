# Contenido
Esta carpeta contiene los Dockerfiles, archivos fuente y scripts de compilacion para las imagenes Docker empleadas por los entornos de desarrollo

# Advertencia
Utilizar solo si no se puede descargar la imagen Docker **educiaafpga/x64** desde Docker Hub empleando `docker pull educiaafpga/x64`. Estos scripts construyen la imagen desde sus fuentes de forma local.

# Instrucciones
Ejecutar el script correspondiente segun la accion a realizar
 - `build_local_x64.sh` : Compila la imagen Docker de 64 bits de forma local
 - `build_remote_x64.sh` : Compila la imagen Docker de 64 bits y la carga a Docker Hub (solo para maintainers del proyecto EDU-CIAA-FPGA).
